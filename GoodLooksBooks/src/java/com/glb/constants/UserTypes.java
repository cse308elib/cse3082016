package com.glb.constants;

/**
 *
 * @author Kevin Young
 */
public enum UserTypes {
    GUEST,
    CUSTOMER,
    ADMIN,
    PUBLISHER;
}
