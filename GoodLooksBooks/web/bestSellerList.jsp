<%-- 
    Document   : bestSellerList
    Created on : May 18, 2016, 3:49:59 AM
    Author     : Hamza
--%>

    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>    

    <head>
        <jsp:include page="/header.jsp" />
        <script src="js/bootstrap-tabcollapse.js"></script>
        <script src="js/customerScript.js"></script>
    </head>
    
    <jsp:include page="/adminNavbar.jsp" />
    <div class="glb-page">
        <jsp:include page="/logo.jsp" />
        <div class="container">
    <br>
    <ul id="tabs" class="nav nav-tabs">
        <li class="active"><a href="#all-wish" data-toggle="tab">Best Sellers</a></li>
    </ul>
    <div id="tabContent" class="tab-content">
        <div class="tab-pane fade in active">
            <c:choose>
                <c:when test="${fn:length(bestSellerlist) > 0}">
                    <table class="results-table">
                        <tr>
                            <c:forEach var="item" items="${bestSellerlist}" varStatus="status">
                                <c:if test="${status.index != 0 && status.index % 4 == 0}">
                                <tr>
                                </tr>
                                </c:if>
                                <td>
                                    <div class="col-xs-6 col-md-3"> 
                                        <td> 
                                            <a href = "BookDescriptionServlet?isbn=${item.isbn}" id="${item.isbn}" class="thumbnail" style="width: 200px;">
                                                <img onload="validateImgUrl(this.id)" name="bookImage" id="item.isbn" class="bookImage" src="${item.imageUrl}" alt="${item.title}" style="width: 200px;">
                                                <p>${item.title}</p>
                                                <h6>by</h6>
                                                <h5>${item.author}</h5>
                                            </a>
                                        </td>
                                    </div>
                                </td>
                        </c:forEach>
                        </tr>
                    </table>
                </c:when>
                <c:otherwise>
                    <div class="heading-box">
                        <h2>There are no bestsellers</h2>
                        <a href="SearchServlet"><h3>Browse some books!</h3></a>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        
    </div>
        </div>
    <br>
</div>